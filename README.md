# Galaxy Conqueror

(Las siguientes instrucciones fueron probadas en Ubunutu 14.04)

### Clonar repositorio Git

Instalar Git

    sudo apt-get install git

Configurar Git

    git config --global user.name "Juan Perez"
    git config --global user.email "juanperez@email.com"

Clonar repositorio

    git clone https://bitbucket.org/yao-ming/galaxy-conquerors.git

Para que el prompt de la terminal muestre el branch actual del repositorio, pegar lo siguiente al final del archivo `~/.bashrc`:

    function parse_git_branch () {
        git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
    }

    RED="\[\033[0;31m\]"
    YELLOW="\[\033[0;33m\]"
    GREEN="\[\033[0;32m\]"
    NO_COLOR="\[\033[0m\]"

    PS1="$GREEN\u@\h$NO_COLOR:\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "

Para que esta configuración tenga efecto inmediatamente, ejecutar:

    source ~/.bashrc

### Instalar Pip

`Pip` es un gestor de paquetes utilizado para instalar y administrar paquetes de software escritos en Python. Para instalar:

    sudo apt-get install python-pip

### Instalar Virtualenv y Virtualenvwrapper

`Virtualenv` es una herramienta para crear entornos virtuales Python. La idea es aislar las dependencias de nuestra aplicación de las del resto del sistema, evitando posibles conflictos.

`Virtualenvwrapper` es simplemente una extensión de virtualenv que presenta una interfaz de comandos más amigable.

Para instalarlos:

    sudo pip install virtualenv
    sudo pip install virtualenvwrapper

*Únicamente en este caso utilizamos*`sudo`*para ejecutar*`pip`*porque queremos instalar virtualenv globalmente en el sistema. Más adelante, cuando se instalen paquetes con*`pip`*en el contexto de un entorno virtual es importante NO USAR*`sudo`*.*

Como últimos pasos hay que crear el directorio que va a contener los entornos virtuales (en este caso el directorio va a ser ~/envs), setear la variable WORKON_HOME, ejecutar el archivo que define los comandos de `virtualenvwrapper` y  agregar algunas líneas al archivo ~/.bashrc para persistir esta configuración:

    export WORKON_HOME=~/envs
    mkdir $WORKON_HOME
    source /usr/local/bin/virtualenvwrapper.sh
    echo "export WORKON_HOME=$WORKON_HOME" >> ~/.bashrc
    echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc


### Instalar dependencias de Galaxy Conqueror

El primer paso será crear un nuevo entorno virtual al que llamaremos GC:

    mkvirtualenv GC

Para activarlo ejecutamos el comando `workon`:

    workon GC

Las dependencias de Galaxy Conqueror están listadas en el archivo `requirements.txt`. Incluye el framework web Django junto a muchos otros paquetes. Para poder ser instalados, algunos de estos paquetes requieren instalar dos dependencias a nivel de sistema:

    sudo apt-get install python-dev
    sudo apt-get install libjpeg8-dev

*El nombre del paquete*`libjpeg8-dev`*podría variar dependiendo de la versión del sistema operativo.*

Ahora sí, para instalar los paquetes en `requirements.txt`, ejecutar:

    pip install -r requirements.txt

*Nótese que se ejecuta el comando `pip` sin `sudo`.*

El comando `pip freeze` lista los paquetes instalados en el entorno virtual actual. El resultado de este comando siempre debería cohincidir con el del archivo requirements.txt, por lo que si se necesitan instalar nuevos paquetes es importante mantener este archivo actualizado para que todos los desarrolladores del proyecto siempre puedan ponerse al día con las dependencias ejecutando el comando anterior. Por ejemplo:

    pip install nombre-del-paquete
    pip freeze > requirements.txt

Es importante que el entorno virtual este activado siempre que se vaya a utilizar la aplicación. No suele ser necesario, pero si en algún momento se lo quiere desactivar se usa el comando `deactivate`.

    deactivate

### Establecer claves secretas

Todo proyecto Django requiere establecer una clave secreta que por razones de seguridad no debe mantenerse en el repositorio. Más información: https://docs.djangoproject.com/en/1.7/ref/settings/#secret-key

Lo mismo ocurre con las claves de la API de Facebook.

Todos los valores secretos deben definirse en el archivo galaxy_conqueror/settings/local_secrets.py. Por ejemplo:

    # -*- coding: utf-8 -*-
    from __future__ import unicode_literals

    SECRET_KEY = '?????????????????????????'

    FACEBOOK_API_SECRETS = {
        # don't remember the purpose of this one
        'PruebaCientifico': '?????????????????????????',
        # to test locally
        'GalaxyConqueror': '?????????????????????????',
        # to use in production
        'CientificosCiudadanos': '?????????????????????????',
    }

La `SECRET_KEY` de Django es un string de al menos 50 caracteres con al menos 5 caracteres distintos. No es necesario que esta clave sea la misma en cada host por lo que esta se puede generar en forma aleatoria.

Para generarla aleatoriamente podemos utilizar el siguiente script Python (https://gist.github.com/mattseymour/9205591):

    import string
    import random

    # Get ascii Characters numbers and punctuation (minus quote characters as they could terminate string).
    chars = ''.join([string.ascii_letters, string.digits, string.punctuation]).replace('\'', '').replace('"', '').replace('\\', '')

    SECRET_KEY = ''.join([random.SystemRandom().choice(chars) for i in range(50)])

    print SECRET_KEY

Las claves de las APIs de Facebook se obtienen en la página de configuración del la aplicación de Facebook correspondiente a Galaxy Conqueror o me las pueden preguntar mandándome un mail a matiascelasco@gmail.com.

### Crear base de datos y ejecutar migraciones

Actualmente estamos utilizando SQLite como motor de base de datos por lo que el archivo de base de datos será creado automáticamente. Al ejecutar las migraciones.

    python manage.py migrate

### Inicializar archivo JSON con una lista vacía de achievements

Eventualmente, la asociación entre los usuarios y sus logros se va a delegar a una API externa de Cientópolis. Por el momento, estamos simulando su comportamiento con un archivo JSON que inicialmente debería consistir en una lista vacía.

    echo "[]" > galaxy_conqueror/apps/badges/user_achievements/galaxy_conqueror.json

