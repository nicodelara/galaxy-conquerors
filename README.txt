

CURRENTLY DEPLOYED IN https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror/


INSTALL GIT


sudo apt-get install git


SETUP GIT

git config --global user.name "YourName YourLastName"
git config --global user.email "youremail"


SHOW CURRENT GIT BRANCH ON LINUX PROMPT

add the following lines to ~/.bashrc

function parse_git_branch () {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

RED="\[\033[0;31m\]"
YELLOW="\[\033[0;33m\]"
GREEN="\[\033[0;32m\]"
NO_COLOR="\[\033[0m\]"

PS1="$GREEN\u@\h$NO_COLOR:\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "


SETUP SSH KEY FOR BITBUCKET

cd ~/.ssh && ssh-keygen
sudo apt-get install xclip
cat id_rsa.pub | xclip -selection c
Go to your bitbucket account >> Click on profile photo >> Manage accounts >> SSH keys >> Add key
Write a label
Paste content from clipboard on "key" field
Click on Add key
exec bash -l  # http://stackoverflow.com/questions/26747455/what-does-this-command-do-exec-bash-l/26747607#26747607
git clone git@bitbucket.org:yao-ming/galaxy-conquerors.git


OPEN SSH SESSION IN socialtv.lifia SERVER

ssh -l matias socialtv.lifia.info.unlp.edu.ar


INSTALL LESS PLUGIN IN SUBLIME TEXT

http://www.desarrolloweb.com/articulos/configuracion-less-sublime-text.html

Warning: The less compiler (lessc) version installed as "apt-get install node-less" in Ubuntu 14.04,
which is lessc 1.4.2 has a bug and throws a non-sense sintax error when importing
external less files.
Perform the following steps in order to avoid this issue by installing a newer version:

sudo apt-get remove node-less
sudo apt-get install npm
sudo npm -g install nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo ln -s /home/matias/.npm/less/1.7.5/package/bin/lessc /usr/bin/lessc
sudo npm install -g less


INSTALL DJANGO

read (or just run) install.sh script


INSTALL apache2 + mod_wsgi

http://www.nickpolet.com/blog/deploying-django-on-aws/1/
