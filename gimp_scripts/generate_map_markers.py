import os

DIR = '/home/matias/dev/galaxy-conquerors/galaxy_conqueror/apps/game/static/game/img/flags/pin'


def get_image_by_name(name):
    for i in gimp.image_list():
        if i.name == name:
            return i
    raise Exception('Image "%s" not found' % name)


layer_names = [
    'inner-border',
    'inner-red',
    'inner-green',
    'inner-white',
    'outer-red',
    'outer-green',
    'outer-white',
]

image = get_image_by_name('pin.xcf')


def set_layers(names):
    for name in layer_names:
        layer = pdb.gimp_image_get_layer_by_name(image, name)
        pdb.gimp_item_set_visible(layer, name in names)


def export(filename, names):
    set_layers(names)
    fullpath = os.path.join(DIR, filename)
    dup = image.duplicate()
    layer = pdb.gimp_image_merge_visible_layers(dup, EXPAND_AS_NECESSARY)
    width = pdb.gimp_drawable_width(layer)
    height = pdb.gimp_drawable_height(layer)
    desired_height = 40
    ratio = desired_height / float(height)
    height = round(height * ratio)
    width = round(width * ratio)
    pdb.gimp_layer_scale(layer, width, height, False)
    pdb.file_png_save(dup, layer, fullpath, filename, 0, 9, 1, 1, 1, 1, 1)


def build_and_export(vote, result):
    if vote is None:
        export('unknown.png', [
            'inner-border',
            'inner-white',
            'outer-white',
        ])
        return
    result_as_str = {
        None: 'unknown',
        True: 'confirmed',
        False: 'rejected',
    }[result]
    vote_as_str = {
        None: 'unknown',
        True: 'positive',
        False: 'negative',
    }[vote]
    filename = "%s-%s.png" % (vote_as_str, result_as_str)
    layers_included = [
        'inner-border',
        {
            None: 'outer-white',
            True: 'outer-green',
            False: 'outer-red',
        }[result],
        'inner-green' if vote else 'inner-red',
    ]
    export(filename, layers_included)

build_and_export(None, None)

for vote in (True, False):
    for result in (True, False, None):
        build_and_export(vote, result)
