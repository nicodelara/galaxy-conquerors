from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.template.base import add_to_builtins

add_to_builtins('django.templatetags.static')


urlpatterns = patterns('', *[
    url(r'', include('social_auth.urls')),
    url(r'^', include('users.urls')),
    url(r'^', include('core.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^play/', include('game.urls')),
    url(r'^badges/', include('badges.urls')),
    url(r'^notifications/', include('notifications.urls')),
    url(r'^tutorial/', include('tutorials.urls')),
    url(r'^reports/', include('reports.urls')),
])

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += patterns('', *[
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        })])

    # this only cares in a development environment
    # in a production environment all the prefix situation is handled by the server itself
    if settings.URL_PREFIX:
        urlpatterns = patterns('', *[
            url('^%s/' % settings.URL_PREFIX[1:], include(urlpatterns)),
        ])
