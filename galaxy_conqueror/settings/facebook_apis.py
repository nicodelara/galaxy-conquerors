# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .local_secrets import FACEBOOK_API_SECRETS

FACEBOOK_APIS = {
    # don't remember the purpose of this one
    'PruebaCientifico': {
        'id': '865746580140252',
        'version': 'v2.4',
    },
    # to test locally
    'GalaxyConqueror': {
        'id': '596228047183450',
        'version': 'v2.3',
    },
    # to use in production
    'CientificosCiudadanos': {
        'id': '864102200328600',
    },
}

# collect api secrets from a non control versioned hidden file
for api_name, data in FACEBOOK_APIS.iteritems():
    try:
        data['secret'] = FACEBOOK_API_SECRETS[api_name]
    except KeyError:
        pass
