# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
from django.core.exceptions import ImproperlyConfigured

try:
    from .local_secrets import SECRET_KEY
except ImportError:
    raise ImproperlyConfigured(
        "A galaxy_conqueror/settings/local_secrets.py file must be defined containing all secret "
        "values like SECRET_KEY. This file MUST NOT be included in a code repository"
    )

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
PROJECT_DIR = os.path.join(BASE_DIR, 'galaxy_conqueror')
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')
sys.path.insert(1, os.path.join(PROJECT_DIR, 'apps'))
sys.path.insert(2, os.path.join(PROJECT_DIR, 'libs'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

USE_DEBUG_TOOLBAR = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django_jinja',
    'django_extensions',

    'social_auth',

    'core',
    'users',
    'game',
    'badges',
    'notifications',
    'tutorials',
    'reports',
)

TEMPLATE_LOADERS = (
    'django_jinja.loaders.AppLoader',
    'django_jinja.loaders.FileSystemLoader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)


DEFAULT_JINJA2_TEMPLATE_EXTENSION = '.jinja'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'galaxy_conqueror.urls'

WSGI_APPLICATION = 'galaxy_conqueror.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}

DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
      'NAME': 'galaxy',
        'USER': 'nicolas',
        'PASSWORD': 'admin',
        'HOST': '127.0.0.1',
        'PORT': '5432',
  }
}

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.user.get_username',
    'social_auth.backends.pipeline.user.create_user',
    'users.pipeline.save_profile',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    'users.pipeline.log_user_activity',
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'users.context_processors.facebook_constants',
)

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.facebook.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)


AUTH_USER_MODEL = 'users.User'
SOCIAL_AUTH_USER_MODEL = AUTH_USER_MODEL

SOCIAL_AUTH_DEFAULT_USERNAME = 'new_facebook_user'
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_FORCE_POST_DISCONNECT = True

'''
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
  'locale': 'ru_RU',
  'fields': 'id, name, email, gender, age_range, picture, user_birthday, user_location, locale, age'
}

SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = ['first_name', 'last_name', 'user_location', 'age_range', 'age', 'locale', 'gender']	

SOCIAL_AUTH_FACEBOOK_SCOPE = ['public_profile', 'email', 'gender', 'age_range']
'''

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

INTERNAL_IPS = ('127.0.0.1',)


# TILE_SERVER_URL = 'https://socialtv.lifia.info.unlp.edu.ar/gc-tiles/server/basemap.tilejson'
#TILE_SERVER_URL = 'http://54.215.209.91/gc-tiles-server/basemap.tilejson'

TILE_SERVER_URL = 'https://cientopolis.lifia.info.unlp.edu.ar/gc-tiles-server/basemap.tilejson'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
URL_PREFIX = ''
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
LOGOUT_URL = '/logout/'
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/play'

STATIC_ROOT = os.path.join(PROJECT_DIR, "public")

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "static"),
)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, "templates"),
)

BADGES_API_APP_ID = 'galaxy_conqueror'

SHELL_PLUS_PRE_IMPORTS = (
    ('badges.models', 'Achievement'),
)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'file_handler': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '%s/logs/info.log' % BASE_DIR,
            'filters': ['require_debug_false'],
            'formatter': 'verbose',
        },
        'console_handler': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_false'],
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console_handler', 'file_handler'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['console_handler', 'file_handler'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}


def calculate_dependent_settings(module, domain, url_prefix, fb_api_name):
    """
    Some settings are calculated in terms of others.

    This function takes care of those dependencies and must
    be used like this:

    import base
    base.calculate_dependent_settings(base,
                                      domain='https://my-url.com'
                                      url_prefix='/my-prefix',
                                      fb_api_name='MyFbApiName')
    from base import *
    """

    module.URL_PREFIX = url_prefix
    module.STATIC_URL = url_prefix + module.STATIC_URL
    module.MEDIA_URL = url_prefix + module.MEDIA_URL
    module.LOGIN_URL = url_prefix + module.LOGIN_URL
    module.LOGIN_REDIRECT_URL = url_prefix + module.LOGIN_REDIRECT_URL
    module.LOGOUT_URL = url_prefix + module.LOGOUT_URL

    from facebook_apis import FACEBOOK_APIS

    if fb_api_name not in FACEBOOK_APIS:
        raise ImproperlyConfigured('Unknown Facebook API "%s"' % fb_api_name)

    module.FACEBOOK_APP_ID = FACEBOOK_APIS[fb_api_name]['id']

    try:
        module.FACEBOOK_API_SECRET = FACEBOOK_APIS[fb_api_name]['secret']
    except KeyError:
        raise ImproperlyConfigured('API secret of the "%s" Facebook not provided' % fb_api_name)

    module.FACEBOOK_EXTENDED_PERMISSIONS = [
        'email',
        'user_friends',
        'user_birthday',
        'user_location',
        #'public_profile',
        #'user_online_presence',
        #'user_likes',
        #'user_about_me',
        #'publish_actions',
        #'friends_location',
    ]

    module.SITE_URL = domain + url_prefix
