import production_base
production_base.calculate_dependent_settings(
    production_base,
    domain='https://cientopolis.lifia.info.unlp.edu.ar',
    url_prefix='/galaxy-conqueror',
    fb_api_name='CientificosCiudadanos',
)
from production_base import *
