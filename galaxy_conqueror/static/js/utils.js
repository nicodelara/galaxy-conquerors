'use strict';

var utils = {
    _prefixedEvents: {
        animationend: [
            'animationend',
            'webkitAnimationEnd',
            'oanimationend',
            'MSAnimationEnd',
        ]
    },
    getPrefixedEvent: function (key) {
        return this._prefixedEvents[key].join(' ');
    }
};