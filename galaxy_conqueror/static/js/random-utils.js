'use strict';

var random = {
    randomFloat: function (min, max) {
        return Math.random() * (max - min) + min;
    },
    randomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    choice: function (sequence) {
        return sequence[this.randomInt(0, sequence.length - 1)];
    }
};