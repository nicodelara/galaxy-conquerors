from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import user_passes_test

from game.models import GalaxyCandidate
from .models import build_heatmap


@user_passes_test(lambda u: u.is_staff)
def report(request):
    return render(request, 'game/report.jinja', {
        'heatmap': build_heatmap(),
        'candidates': GalaxyCandidate.objects.all(),
        'URL_PREFIX': settings.URL_PREFIX,
        'navbar_report': True,
    })


@user_passes_test(lambda u: u.is_staff)
def confirm_galaxy(request, galaxy_id):
    galaxy = get_object_or_404(GalaxyCandidate, id=galaxy_id)
    galaxy.confirm(request.user)
    return redirect('report')


@user_passes_test(lambda u: u.is_staff)
def reject_galaxy(request, galaxy_id):
    galaxy = get_object_or_404(GalaxyCandidate, id=galaxy_id)
    galaxy.reject(request.user)
    return redirect('report')
