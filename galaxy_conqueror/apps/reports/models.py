import csv
import datetime
from pprint import pprint
from collections import defaultdict, Counter

from django.conf import settings
from game.helpers import get_galaxy_coordinates_at_image
from game.models import GalaxyCandidate


def build_heatmap():

    def generate_coords(x, y, z):
        size = 2 ** (6 - z)
        for xx in range(size * x, size * x + size):
            for yy in range(size * y, size * y + size):
                yield xx, yy

    path = settings.BASE_DIR + "/tile-requests.csv"
    with open(path, 'rb') as f:
        reader = csv.reader(f)
        counter = defaultdict(lambda: defaultdict(lambda: Counter()))
        reader.next()  # ignore first row (headers)
        ip_counter = Counter()
        ip_min_date = dict()
        ip_max_date = dict()
        for row in reader:
            ip, timestamp, _, z, x, y = row
            date = datetime.datetime.strptime(timestamp, "[%d/%b/%Y:%H:%M:%S").date()
            if date != datetime.date(2015, 10, 14):
                continue
            ip_min_date[ip] = min(ip_min_date.setdefault(ip, date), date)
            ip_max_date[ip] = max(ip_max_date.setdefault(ip, date), date)
            ip_counter[ip] += 1
            x, y, z = map(int, (x, y, z))
            counter[z][x][y] += 1
            if z > 3:
                for x, y in generate_coords(x, y, z):
                    counter['summary'][x][y] += 1
        ip_info = {}
        for ip in ip_counter.keys():
            ip_info[ip] = {
                'count': ip_counter[ip],
                'min_date': ip_min_date[ip],
                'max_date': ip_max_date[ip],
            }
        pprint(ip_info)

        heatmap_data = {}
        for level in counter.iterkeys():
            heatmap_data[level] = []
            for x in counter[level].iterkeys():
                for y in counter[level][x].iterkeys():
                    heatmap_data[level].append({
                        'x': x,
                        'y': y,
                        'count': counter[level][x][y],
                    })

        heatmap_data['galaxies'] = []
        for galaxy in GalaxyCandidate.objects.all():
            if galaxy.is_confirmed():
                point = get_galaxy_coordinates_at_image(galaxy)
                heatmap_data['galaxies'].append({
                    'x': point.real / 6101.0,
                    'y': point.imag / 6000.0,
                })

        return heatmap_data
