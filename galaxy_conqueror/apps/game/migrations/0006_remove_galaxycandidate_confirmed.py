# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0005_galaxycandidate_confirmed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='galaxycandidate',
            name='confirmed',
        ),
    ]
