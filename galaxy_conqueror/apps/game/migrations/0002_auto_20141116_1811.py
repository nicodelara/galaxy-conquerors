# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alien',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'aliens')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='badge',
            name='owners',
            field=models.ManyToManyField(related_name='badges', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='galaxycandidate',
            name='owner',
            field=models.ForeignKey(related_name='galaxy_candidates', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='vote',
            name='galaxy_candidate',
            field=models.ForeignKey(related_name='votes', to='game.GalaxyCandidate'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='vote',
            name='owner',
            field=models.ForeignKey(related_name='votes', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
