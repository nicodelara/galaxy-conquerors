# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0009_galaxycandidate_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='galaxycandidate',
            name='thumbnail',
            field=models.ImageField(null=True, upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
