# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0011_update_galaxy_thumbnails'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Alien',
        ),
    ]
