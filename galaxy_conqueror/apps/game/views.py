from jsonview.decorators import json_view
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from .models import Game
from ajax_api import GameAPI
from notifications.models import Notification
from tutorials.models import TutorialStats
from utils import log_errors


@login_required
def game(request):
    game = Game(request.user)
    tutorial_stats, _ = TutorialStats.objects.get_or_create(user=request.user)
    return render(request, 'game/game.jinja', {
        'game': game,
        'navbar_playing': True,
        'unread_notifications_as_dict': Notification.unread_for_user_as_dicts(request.user),
        'should_take_tutorial': tutorial_stats.should_take_tutorial(),
    })


@json_view
@log_errors
@login_required
def ajax_api(request):
    data = dict(request.POST)
    api = GameAPI(request.user)
    action_function = {
        'vote': api.vote,
        'mark_galaxy': api.mark_galaxy,
        'alien_hunted': api.alien_hunted,
    }[data.pop('action')[0]]
    for k, v in data.iteritems():
        data[k] = v[0]
    response = action_function(**data) or {}
    response['unread_notifications'] = Notification.unread_for_user_as_dicts(request.user)
    return response
