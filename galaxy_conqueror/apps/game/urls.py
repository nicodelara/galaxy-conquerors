from django.conf.urls import patterns, url


urlpatterns = patterns('game.views', *[

    url(r'^$', 'game', name='game'),
    url(r'^api/$', 'ajax_api', name='game_api'),

])
