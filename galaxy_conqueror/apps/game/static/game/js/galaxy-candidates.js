/*globals
    jQuery,
    _,
    ol,
    data,
    window,
    flagImages,
*/

var galaxyCandidates = (function ($, _, ol, data, setTimeout) {

    'use strict';

    var templates,
        popupOverlay,
        popupContainer,
        popupContent,
        map,
        imagesUrl,
        flagsOverlay,
        currentGalaxyId;

    // galaxt candidate features layer--------------------------------------------------------------
    function buildGalaxyCandidateFeature(coords, id) {
        return new ol.Feature({
            geometry: new ol.geom.Point(coords),
            size: flagImages.size,
            type: 'flag',
            cursor: 'pointer',
            id: id,
        });
    }

    function buildFlagsOverlay() {
        var source = new ol.source.Vector();

        source.addFeatures($.map(data.getAllGalaxyCandidates(), function (candidate) {
            return buildGalaxyCandidateFeature([candidate.x, candidate.y], candidate.id);
        }));

        return new ol.layer.Vector({
            source: source,
            style: function (feature) {
                var properties = feature.getProperties(),
                    galaxy = data.getGalaxyCandidate(properties.id),
                    currentUserVoteStatus,
                    expertVoteStatus,
                    src;

                if (galaxy.isConfirmed) {
                    expertVoteStatus = 'confirmed';
                } else if (galaxy.isRejected) {
                    expertVoteStatus = 'rejected';
                } else {
                    expertVoteStatus = 'unknown';
                }

                if (galaxy.currentUserVote === true) {
                    currentUserVoteStatus = 'positive';
                } else if (galaxy.currentUserVote === false) {
                    currentUserVoteStatus = 'negative';
                } else {  // null
                    currentUserVoteStatus = 'unknown';
                }

                src = imagesUrl + flagImages.filenames[currentUserVoteStatus][expertVoteStatus];

                return [new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: flagImages.anchor,
                        size: [properties.size[0], properties.size[1]],
                        scale: 1,
                        src: src,
                    })
                })];
            }
        });
    }

    // go to galaxy---------------------------------------------------------------------------------
    function disableNextGalaxyButton() {
        $('.nextGalaxy').addClass('disabled')
                        .find('button').prop('disabled', true);
    }

    function setZoom(newZoom) {
        map.getControls().getArray()[0].zoomByDelta_(newZoom - map.getView().getZoom());
    }

    function goToPoint(coords) {
        var view = map.getView(),
            start = +new Date(),
            bouncingRate = Math.max(2 * view.getResolution(), 30000),
            duration = Math.max(700, Math.min(bouncingRate / view.getResolution() * 500, 2000)),
            pan,
            bounce;

        pan = ol.animation.pan({
            duration: duration,
            source: view.getCenter(),
            start: start
        });
        bounce = ol.animation.bounce({
            duration: duration,
            resolution: bouncingRate,
            start: start
        });
        map.beforeRender(pan, bounce);
        view.setCenter(coords);

        setTimeout(function () {
            setZoom(6);
        }, duration);
    }

    function goToGalaxyCandidate(galaxyId) {
        var galaxy = data.getGalaxyCandidate(galaxyId);
        goToPoint([galaxy.x, galaxy.y]);
    }

    function getNextUnvisitedGalaxyId() {
        // TODO: improve this O(n) algorithm
        var max = null, nextGalaxyId = null;
        $.each(data.getAllGalaxyCandidates(), function (i, galaxy) {
            if (galaxy.canVote) {
                if (max === null || galaxy.positives - galaxy.negatives > max) {
                    nextGalaxyId = galaxy.id;
                    max = galaxy.positives - galaxy.negatives;
                }
            }
        });
        return nextGalaxyId;
    }



    function goToNextUnvisitedGalaxy() {
        var id = getNextUnvisitedGalaxyId();
        if (id !== null) {
            goToGalaxyCandidate(id);
        }
    }

    // popup----------------------------------------------------------------------------------------
    function voteHandlerFunction(isPositive) {
        return function () {
            data.vote(currentGalaxyId, isPositive, function () {
                var galaxy = data.getGalaxyCandidate(currentGalaxyId);
                popupContent.innerHTML = templates.popup(galaxy);
                if (getNextUnvisitedGalaxyId() === null) {
                    disableNextGalaxyButton();
                }
                flagsOverlay.changed();
            });
        };
    }

    function openGalaxyPopup(feature) {
        currentGalaxyId = feature.getProperties().id;
        var coord = feature.getGeometry().getCoordinates(),
            resolution = map.getView().getResolution(),
            galaxy = data.getGalaxyCandidate(currentGalaxyId);
        coord[0] = (parseInt(coord[0], 10) - 6 * resolution).toString();
        coord[1] = (parseInt(coord[1], 10) + 45 * resolution).toString();
        popupOverlay.setPosition(coord);
        popupContent.innerHTML = templates.popup(galaxy);
    }

    function closeGalaxyPopup() {
        popupOverlay.setPosition(undefined);
    }

    function buildPopupOverlay() {
        popupContainer = $('#popup')[0];
        popupContent = $('#popup-content')[0];
        $(popupContent).on('click', '.votes .positive', voteHandlerFunction(true));
        $(popupContent).on('click', '.votes .negative', voteHandlerFunction(false));
        return new ol.Overlay({
            element: popupContainer,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });
    }


    // add galaxy-----------------------------------------------------------------------------------
    function addGalaxyCandidateAt(coords) {
        var galaxyCandidateId = data.addGalaxyCandidate(coords);
        flagsOverlay.getSource().addFeature(buildGalaxyCandidateFeature(coords, galaxyCandidateId));
    }


    return {
        init: function (params) {

            imagesUrl = params.imagesUrl;
            flagImages.init({
                imagesUrl: imagesUrl,
            });

            currentGalaxyId = null;

            map = params.map;

            flagsOverlay = buildFlagsOverlay();
            popupOverlay = buildPopupOverlay();

            templates = {
                popup: _.template($("#popup-template").html()),
            };

            map.addOverlay(popupOverlay);
            map.addOverlay(flagsOverlay);

            return this;
        },
        goToNextUnvisitedGalaxy: goToNextUnvisitedGalaxy,
        getNextUnvisitedGalaxyId: getNextUnvisitedGalaxyId,
        goToGalaxyCandidate: goToGalaxyCandidate,
        goToPoint: goToPoint,
        addGalaxyCandidateAt: addGalaxyCandidateAt,
        openGalaxyPopup: openGalaxyPopup,
        closeGalaxyPopup: closeGalaxyPopup,
    };
}(jQuery, _, ol, data, window.setTimeout));