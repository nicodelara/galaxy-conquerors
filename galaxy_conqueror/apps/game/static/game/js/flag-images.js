/*globals
    _,
*/
var flagImages = (function () {

    'use strict';

    var imagesUrl,
        type;

    function FlagType(args) {
        var dir = 'flags/' + args.directory;
        this.anchor = args.anchor;
        this.size = args.size;
        this.filenames = {
            positive: {
                confirmed: dir + '/positive-confirmed.png',
                rejected: dir + '/positive-rejected.png',
                unknown: dir + '/positive-unknown.png',
            },
            negative: {
                confirmed: dir + '/negative-confirmed.png',
                rejected: dir + '/negative-rejected.png',
                unknown: dir + '/negative-unknown.png',
            },
            unknown: {
                confirmed: dir + '/unknown.png',
                rejected: dir + '/unknown.png',
                unknown: dir + '/unknown.png',
            },
        };
        this.cursorOffset = [
            this.size[0] * this.anchor[0],
            this.size[1] * this.anchor[1],
        ];
        this.cursor = _.template('url(<%= url %>) <%= x %> <%= y %>, default')({
            url: imagesUrl + this.filenames.positive.unknown,
            x: this.cursorOffset[0],
            y: this.cursorOffset[1],
        });
    }


    return {
        init: function (params) {
            imagesUrl = params.imagesUrl;
            // type = new FlagType({
            //     directory: '3d',
            //     anchor: [0.4, 0.9375],
            //     size: [40, 47],
            // });
            type = new FlagType({
                directory: 'pin2',
                anchor: [0.5, 1],
                size: [25, 40],
            });
            this.cursor = type.cursor;
            this.anchor = type.anchor;
            this.filenames = type.filenames;
            this.size = type.size;
        },
    };

}());
