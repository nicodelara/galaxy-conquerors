/*globals
    jQuery,
    utils,
    data,
    ol,
    random,
    window,
*/

var aliens = (function ($, utils, data, ol, random, setTimeout, setInterval) {

    'use strict';

    var i,
        map,
        extent,
        overlay,
        alienImages,
        ALIENS_INITIAL_AMOUNT = 50,
        ALIENS_INTERVAL = 3000,
        ALIENS_REPLACE_AMOUNT_PER_INTERVAL = 25;

    function randomPoint() {
        var coords = [
            random.randomFloat(extent[0], extent[2]),
            random.randomFloat(extent[1], extent[3])
        ];
        return new ol.geom.Point(coords);
    }

    function killAlien(feature, pixel) {
        overlay.getSource().removeFeature(feature);
        var x = pixel[0],
            y = pixel[1],
            $score = $($('.player-stats li')[1]).find('span'),
            mapPosition = $('#map').offset(),
            $span = $('<span>')
                .addClass('expanding-point')
                .css('left', mapPosition.left + x)
                .css('top', mapPosition.top + y)
                .text('+10');

        $('body').append($span);
        $span.animate({
            fontSize: 150,
            left: x - 100,
            top: y - 25
        }, 100)
        .animate({
            left: $score.offset().left,
            top: $score.offset().top,
            fontSize: 10,
            opacity: 0
        }, 300)
        setTimeout(function () {
            data.alienHunted(10, map.getCoordinateFromPixel(pixel));
            $score.text(data.getUserScore());
        }, 400);
    }

    function randomAlienFeature() {
        var image = random.choice(alienImages);
        return new ol.Feature({
            geometry: randomPoint(),
            src: image.src,
            size: image.size,
            type: 'alien',
            cursor: 'crosshair'
        });
    }

    function replaceSomeAliens() {
        var source = overlay.getSource();
        for (i = 0; i < ALIENS_REPLACE_AMOUNT_PER_INTERVAL; i += 1) {
            source.removeFeature(random.choice(source.getFeatures()));
        }
        for (i = 0; i < ALIENS_REPLACE_AMOUNT_PER_INTERVAL; i += 1) {
            source.addFeature(randomAlienFeature());
        }
    }

    function buildOverlay() {
        var randomAliens = [],
            source = new ol.source.Vector();
        for (i = 0; i < ALIENS_INITIAL_AMOUNT; i += 1) {
            randomAliens.push(randomAlienFeature());
        }
        source.addFeatures(randomAliens);

        return new ol.layer.Vector({
            source: source,
            extent: extent,
            style: function (feature, resolution) {
                var properties = feature.getProperties();
                if (resolution > 7000) {
                    return [];
                }
                // TODO: change opacity depending on zoom
                return [new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: [0.5, 0.5],
                        size: [properties.size[0], properties.size[1]],
                        scale: 2000 / resolution,
                        src: properties.src,
                    })
                })];
            }
        });
    }

    function init(params) {
        map = params.map;
        extent = params.extent;

        alienImages = params.alienImages;
        if (alienImages.length === 0) {
            throw "Alien images missing";
        }

        overlay = buildOverlay();
        map.addOverlay(overlay);

        setInterval(replaceSomeAliens, ALIENS_INTERVAL);
    }

    return {
        init: init,
        killAlien: killAlien,
    };


}(jQuery, utils, data, ol, random, window.setTimeout, window.setInterval));
