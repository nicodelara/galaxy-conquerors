/*globals
    d3,
    jQuery
*/


var heatmap = (function ($) {

    'use strict';

    var width = 610,
        height = 600,
        level = 6,
        $opacityInput = $('input[name=opacity]'),
        initialOpacity = $opacityInput.val(),
        serie = 'summary';

    function colorScale(domain, range) {
        // TODO: find out if d3 has a mechanism for defining custom scales
        var rScale = d3.scale.linear()
                       .domain(domain)
                       .range([range[0][0], range[1][0]]),
            gScale = d3.scale.linear()
                       .domain(domain)
                       .range([range[0][1], range[1][2]]),
            bScale = d3.scale.linear()
                       .domain(domain)
                       .range([range[0][2], range[1][2]]);
        return function (domainValue) {
            return 'rgb(' + [
                Math.round(rScale(domainValue)),
                Math.round(gScale(domainValue)),
                Math.round(bScale(domainValue)),
            ].join(', ') + ')';
        };
    }

    function drawTiles(svg, width, height, data) {
        var xScale,
            yScale,
            maxValue,
            tileColorScale,
            numberOfRows = Math.pow(2, level),
            numberOfColumns = Math.pow(2, level),
            tileWidth = width / numberOfColumns,
            tileHeight = height / numberOfRows;

        xScale = d3.scale.linear()
                   .domain([0, numberOfColumns - 1])
                   .range([0, width - tileWidth]);

        yScale = d3.scale.linear()
                   .domain([0, numberOfRows - 1])
                   .range([0, height - tileHeight]);

        maxValue = d3.max(data, function (d) {
            return d.count;
        });
        tileColorScale = colorScale([0, maxValue], [[0, 0, 255], [255, 0, 0]]);

        svg.selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .classed('tile', true)
            .attr('x', function (d) {
                return xScale(d.x);
            })
            .attr('y', function (d) {
                return yScale(d.y);
            })
            .attr('width', tileWidth)
            .attr('height', tileHeight)
            .attr('title', function (d) {
                return d.count;
            })
            .attr('fill', function (d) {
                return tileColorScale(d.count);
            })
            .attr('opacity', initialOpacity);
    }

    function drawDots(svg, width, height, data) {
        var xScale, yScale;

        xScale = d3.scale.linear()
                   .domain([0, 1])
                   .range([0, width]);

        yScale = d3.scale.linear()
                   .domain([0, 1])
                   .range([0, height]);

        svg.selectAll('circle')
            .data(data)
            .enter()
            .append('circle')
            .attr('cx', function (d) {
                return xScale(d.x);
            })
            .attr('cy', function (d) {
                return yScale(d.y);
            })
            .attr('r', 3)
            .attr('fill', 'rgb(144, 212, 29)');
    }


    function init(params) {
        var svg;

        svg = d3.select('svg').attr('width', width).attr('height', height);
        drawTiles(svg, width, height, params.data[serie]);
        drawDots(svg, width, height, params.data.galaxies);
        $opacityInput.on('change', function () {
            svg.selectAll('.tile').attr('opacity', $(this).val());
        });

    }

    return {
        init: init,
    };

}(jQuery));