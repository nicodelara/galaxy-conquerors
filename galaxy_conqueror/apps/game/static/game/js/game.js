/*globals
    jQuery,
    ol,
    galaxyCandidates,
    aliens,
    flagImages,
    controls,
    data,
    tutorial,
    setTimeout,
    bootbox,
    _,
*/

var game = (function ($, ol, galaxyCandidates, aliens, controls, data, tutorial, flagImages) {

    'use strict';

    var map,
        clickMode,
        markGalaxyClickMode,
        defaultClickMode,
        tilesServerUrl,
        minResolution = 611.49622628141;

    function MarkGalaxyClickMode() {
        return {
            onClick: function (evt) {
                galaxyCandidates.addGalaxyCandidateAt(evt.coordinate);
                galaxyCandidates.closeGalaxyPopup();
                clickMode = defaultClickMode;
                $('.ol-control.markGalaxy button div').toggleClass("fa-trash", false);
                $('.ol-control.markGalaxy button div').toggleClass("fa-crosshairs", true);
                $('.ol-zoom-in, .ol-zoom-out').tooltip({placement: 'right'});

            },
            getCursor: function () {
                return flagImages.cursor;
            },
        };
    }

    function DefaultClickMode() {
        return {
            onClick: function (evt) {
                if (map.hasFeatureAtPixel(evt.pixel)) {
                    map.forEachFeatureAtPixel(evt.pixel, function (feature) {
                        switch (feature.getProperties().type) {
                        case 'flag':
                            galaxyCandidates.openGalaxyPopup(feature);
                            break;
                        case 'alien':
                            aliens.killAlien(feature, evt.pixel);
                            break;
                        }
                    });
                } else {
                    galaxyCandidates.closeGalaxyPopup();
                }
            },
            getCursor: function (pixel) {
                return map.forEachFeatureAtPixel(pixel, function (feature) {
                    return feature.getProperties().cursor;
                });
            },
        };
    }

    function clickHandler(evt) {
        clickMode.onClick(evt);
    }

    function mouseMoveHandler(evt) {
        var pixel = map.getEventPixel(evt.originalEvent);
        map.getTarget().style.cursor = clickMode.getCursor(pixel) || 'default';
    }

    function changeResolutionHandler() {
        galaxyCandidates.closeGalaxyPopup();
    }

    function buildTileLayer(source, extent) {
        return new ol.layer.Tile({
            title: "Global Imagery",
            source: source,
            extent: extent,
        });
    }

    function buildView(extent) {
        var w = $('#map').width(),
            h = $('#map').height();
        return new ol.View({
            center: [0, 0],
            zoom: 3,
            minZoom: 3,
            maxZoom: 8,  // if this change the extent HAS to change too
            extent: [
                // adjusted for max zoom
                extent[0] + w * minResolution,
                extent[1] + h * minResolution,
                extent[2] - w * minResolution,
                extent[3] - h * minResolution,
            ],
        });
    }

    function buildControls() {
        var defaults = ol.control.defaults({
                attributionOptions: ({
                    collapsible: true
                })
            });
        return defaults.extend([
            new controls.MarkGalaxyControl({
                onClick: function (evt) {
                    if (clickMode === markGalaxyClickMode) {
                        clickMode = defaultClickMode;
                        /*var $icon =  $(evt.target).children()[0];*/
                        var $icon = $('.ol-control.markGalaxy button div');
                        $icon.toggleClass("fa-trash", false);
                        $icon.toggleClass("fa-crosshairs", true);

                    } else {
                        clickMode = markGalaxyClickMode;
                        /*var $icon =  $(evt.target).children()[0];*/
                        var $icon = $('.ol-control.markGalaxy button div');
                        $icon.toggleClass("fa-crosshairs", false);
                        $icon.toggleClass("fa-trash", true);
                    }
                }
            }),
            new controls.TutorialControl({
                onClick: tutorial.open
            }),
            new controls.ScoreControl({
                initialScore: data.getUserScore(),
            }),
            new controls.NextUnvisitedGalaxyControl({
                onClick: galaxyCandidates.goToNextUnvisitedGalaxy,
                disabled: galaxyCandidates.getNextUnvisitedGalaxyId() === null,
            }),
        ]);
    }

    function buildSource() {
        var source = new ol.source.TileJSON({
            url: tilesServerUrl,
            crossOrigin: 'anonymous'
        });
        source.on('change', function () {
            if (source.getState() === 'error') {
                var msg = _.template($('#ssl-error-msg-template').html())({
                    tilesServerUrl: tilesServerUrl,
                });
                bootbox.alert(msg);
            }
        });
        return source;
    }

    function buildMap(source, extent) {
        return new ol.Map({
            target: $('#map')[0],
            view: buildView(extent),
            layers: [buildTileLayer(source, extent)],
            controls: buildControls(),
        });
    }

    return {
        init: function (params) {

            var source,
                extent;

            tilesServerUrl = params.tilesServerUrl;

            data.init({
                galaxyCandidates: params.galaxyCandidates,
                ajaxUrl: params.ajaxUrl,
                currentUserId: params.currentUserId,
                users: params.users,
            });

            markGalaxyClickMode = new MarkGalaxyClickMode();
            defaultClickMode = new DefaultClickMode();
            clickMode = defaultClickMode;

            source = buildSource();
            extent = source.getProjection().getExtent();

            map = buildMap(source, extent);

            galaxyCandidates.init({
                imagesUrl: params.imagesUrl,
                flagType: params.flagType,
                map: map,
            });

            aliens.init({
                map: map,
                alienImages: params.alienImages,
                extent: extent,
            });

            map.on('click', clickHandler);
            map.getView().on('change:resolution', changeResolutionHandler);
            $(map.getViewport()).on('mousemove', mouseMoveHandler);

            $('.ol-zoom .ol-zoom-in').html('<i class="fa fa-search-plus"></i>');
            $('.ol-zoom .ol-zoom-out').html('<i class="fa fa-search-minus"></i>');

            return this;
        }
    };
}(jQuery, ol, galaxyCandidates, aliens, controls, data, tutorial, flagImages));