/*globals
    jQuery,
    ko,
*/

var data = (function ($, ko) {

    'use strict';

    var i,
        galaxyCandidatesById,
        users,
        usersModel,
        currentUserId,
        ajaxUrl,
        galaxyFakeIdToId,
        nextGalaxyFakeId;

    function buildTable(objects) {
        var table = {};
        for (i = 0; i < objects.length; i += 1) {
            table[objects[i].id] = objects[i];
        }
        return table;
    }

    function getGalaxyCandidate(galaxyId) {
        var g;
        // if (galaxyId < 0) {
        //     galaxyId = galaxyFakeIdToId[galaxyId];
        // }
        g = galaxyCandidatesById[galaxyId];
        for (i = 0; i < users.length; i += 1) {
            if (users[i].id  === g.ownerId) {
                g.owner = users[i];
            }
        }
        return g;
    }

    function getUserById(userId) {
        for (i = 0; i < users.length; i += 1) {
            if (users[i].id  === userId) {
                return users[i];
            }
        }
    }

    return {

        init: function (params) {

            galaxyCandidatesById = buildTable(params.galaxyCandidates);
            users = [];
            for (i = 0; i < params.users.length; i += 1) {
                users.push({
                    id: params.users[i].id,
                    displayedName: params.users[i].displayedName,
                    levelName: params.users[i].levelName,
                    isConnectedToFacebook: params.users[i].isConnectedToFacebook,
                    rankingPosition: ko.observable(params.users[i].rankingPosition),

                    score: ko.observable(params.users[i].score),
                });
            }
            ajaxUrl = params.ajaxUrl;
            currentUserId = params.currentUserId;
            galaxyFakeIdToId = {};
            nextGalaxyFakeId = -1;

            // Overall viewmodel for this screen, along with initial state
            function UsersViewModel() {
                // Editable data
                this.users = ko.observableArray(users);
                var self = this;
                this.sort = function () {
                    self.users.sort(function (a, b) {
                        return b.score() - a.score();
                    });
                    for (i = 0; i < self.users().length; i += 1) {
                        if (i === 0 || self.users()[i - 1].score() !== self.users()[i].score()) {
                            self.users()[i].rankingPosition(i + 1);
                        } else {
                            self.users()[i].rankingPosition(self.users()[i - 1].rankingPosition());
                        }
                    }
                };
                this.sort();
            }
            usersModel = new UsersViewModel();
            ko.applyBindings(usersModel);
        },

        alienHunted: function (offset, pixel) {
            for (i = 0; i < users.length; i += 1) {
                if (users[i].id  === currentUserId) {
                    users[i].score(users[i].score() + offset);
                }
            }
            usersModel.sort();
            $.ajax(ajaxUrl, {
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'alien_hunted',
                    x: pixel[0],
                    y: pixel[1],
                },
            });
        },

        getUserName: function () {
            return getUserById(currentUserId).displayedName;
        },

        getUserScore: function () {
            return getUserById(currentUserId).score();
        },

        addGalaxyCandidate: function (coords) {
            var fakeId = nextGalaxyFakeId;
            galaxyCandidatesById[fakeId] = {
                id: fakeId,
                x: coords[0],
                y: coords[1],
                positives: 0,
                negatives: 0,
                ownerId: currentUserId,
                canVote: false,
                currentUserVote: true,
            };
            $.ajax(ajaxUrl, {
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'mark_galaxy',
                    x: coords[0],
                    y: coords[1],
                },
                success: function (response) {
                    galaxyFakeIdToId[fakeId] = response.id;
                    galaxyCandidatesById[fakeId].thumbnailUrl = response.thumbnailUrl;
                }
            });
            nextGalaxyFakeId -= 1;
            return fakeId;
        },

        getGalaxyCandidate: getGalaxyCandidate,

        vote: function (galaxyId, isPositive, callback) {
            $.ajax(ajaxUrl, {
                type: 'post',
                dataType: 'json',
                data: {
                    action: 'vote',
                    galaxy_id: galaxyId,
                    is_positive: isPositive,
                },
                success: function (response) {
                    var updatedScore, galaxyCandidate = galaxyCandidatesById[galaxyId];
                    galaxyCandidate.canVote = response.can_vote;
                    galaxyCandidate.isConfirmed = response.is_confirmed;
                    galaxyCandidate.isRejected = response.is_rejected;
                    galaxyCandidate.currentUserVote = response.current_user_vote;
                    galaxyCandidate.positives = response.positives;
                    galaxyCandidate.negatives = response.negatives;
                    for (i = 0; i < response.updated_scores.length; i += 1) {
                        updatedScore = response.updated_scores[i];
                        getUserById(updatedScore.id).score(updatedScore.score);
                    }
                    $('#score').text(data.getUserScore());
                    usersModel.sort();
                    callback();
                },
                error: function () {
                    callback();
                }
            });
        },

        getAllGalaxyCandidates: function () {
            var id, gs = [];
            for (id in galaxyCandidatesById) {
                if (galaxyCandidatesById.hasOwnProperty(id)) {
                    gs.push(galaxyCandidatesById[id]);
                }
            }
            return gs;
        }

    };

}(jQuery, ko));