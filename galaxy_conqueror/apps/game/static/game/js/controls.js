/*globals
    jQuery,
    ol,
*/

var controls = (function ($, ol) {

    'use strict';

    function MarkGalaxyControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.append($('<div>').addClass('fa fa-crosshairs'));
        var $div = $('<div>')
                .addClass('markGalaxy')
                .addClass('ol-control')
                .append($button);

        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(MarkGalaxyControl, ol.control.Control);

    function NextUnvisitedGalaxyControl(opt_options) {
        var options = opt_options || {},
            $button = $('<button>').text('Siguiente Galaxia'),
            $div = $('<div>')
                .addClass('nextGalaxy')
                .addClass('ol-control')
                .append($button);

        if (options.disabled) {
            $button.prop('disabled', true);
            $div.addClass('disabled');
        }

        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(NextUnvisitedGalaxyControl, ol.control.Control);

    function ScoreControl(opt_options) {
        var options = opt_options || {},
            $label = $('<span>').text(options.initialScore).attr('id', 'score'),
            $div = $('<div>')
                .addClass('score')
                .addClass('ol-control')
                .append($label);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(ScoreControl, ol.control.Control);

    function TutorialControl(opt_options) {
        var options = opt_options || {};
        var $button = $('<button>');
        $button.append($('<div>').addClass('fa fa-info'));
        var $label = $button.attr('id', 'open-tutorial-btn');
        var $div = $('<div>').addClass('open-tutorial-btn')
                .addClass('open-tutorial-btn')
                .addClass('ol-control')
                .append($label);
        $button.on('click', options.onClick);
        ol.control.Control.call(this, {
            element: $div[0],
            target: options.target
        });
    }
    ol.inherits(TutorialControl, ol.control.Control);


    return {
        MarkGalaxyControl: MarkGalaxyControl,
        ScoreControl: ScoreControl,
        TutorialControl: TutorialControl,
        NextUnvisitedGalaxyControl: NextUnvisitedGalaxyControl,
    };

}(jQuery, ol));