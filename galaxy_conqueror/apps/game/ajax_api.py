from decimal import Decimal
from django.shortcuts import get_object_or_404
from models import GalaxyCandidate, Vote, AlienHunted


class GameAPI(object):

    def __init__(self, user):
        self.user = user

    def vote(self, galaxy_id, is_positive):
        if is_positive == 'false':
            is_positive = False
        else:
            is_positive = True
        galaxy = get_object_or_404(GalaxyCandidate, id=galaxy_id)
        current_user_vote = Vote.objects.create(
            galaxy_candidate=galaxy,
            owner=self.user,
            is_positive=is_positive,
        )

        def build_dict(user):
            return {
                'id': user.id,
                'score': user.score,
            }

        updated_scores = [build_dict(vote.owner) for vote in galaxy.votes.all()]
        updated_scores.append(build_dict(galaxy.owner))

        return {
            'can_vote': False,
            # Only admins care whether the decision is disputed or not.
            # For players, disputed candidates are presented as confirmed galaxies
            # TODO: bad smell. This logic is repeated in other parts of the code.
            # Those parts can be found by searching for this exactly same comment
            'is_confirmed': galaxy.is_confirmed() or galaxy.is_disputed(),
            'is_rejected': galaxy.is_rejected(),
            'current_user_vote': current_user_vote.is_positive,
            'positives': galaxy.positives_votes_count(),
            'negatives': galaxy.negatives_votes_count(),
            'updated_scores': updated_scores,
        }

    def cancel_vote(self, galaxy_id):
        galaxy = get_object_or_404(GalaxyCandidate, id=galaxy_id)
        Vote.objects.get(galaxy_candidate=galaxy, owner=self.user).delete()

    def __update_score(self, offset):
        self.user.score += offset
        self.user.save()

    def alien_hunted(self, x, y):
        self.__update_score(10)
        AlienHunted.objects.create(hunter=self.user, x=x, y=y)

    def mark_galaxy(self, x, y):
        galaxy = GalaxyCandidate.objects.create(owner=self.user, x=x, y=y)
        galaxy.x = Decimal(galaxy.x)
        galaxy.y = Decimal(galaxy.y)
        galaxy.update_thumbnail()
        return {
            'id': galaxy.id,
            'thumbnailUrl': galaxy.thumbnail_url(),
        }
