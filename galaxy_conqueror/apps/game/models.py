import math
from django.db import models
from users.models import User
from utils import nice_reverse as reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import F
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from .helpers import create_galaxy_thumbnail

class GalaxyCandidate(models.Model):

    CONFIRMED = 'Confirmado'
    REJECTED = 'Rechazado'
    DISPUTED = 'Disputado'
    PENDING = 'Pendiente'

    owner = models.ForeignKey(User, related_name='galaxy_candidates')
    x = models.DecimalField(max_digits=15, decimal_places=2)
    y = models.DecimalField(max_digits=15, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(blank=True, null=True)

    def thumbnail_url(self):
        try:
            return settings.URL_PREFIX + self.thumbnail.url
        except ValueError:
            return staticfiles_storage.url("img/not-available.jpg")

    def __unicode__(self):
        return "Discovered by user %d at (%.2f, %.2f)" % (self.owner_id, self.x, self.y)

    def status(self):
        # if self.owner.is_staff:
        #     return GalaxyCandidate.CONFIRMED

        votes = Vote.qualified_votes().filter(galaxy_candidate=self)
        positives = votes.filter(is_positive=True).count()
        negatives = votes.filter(is_positive=False).count()

        if negatives > 0 and positives > 0:
            return GalaxyCandidate.DISPUTED
        elif negatives > 0:
            return GalaxyCandidate.REJECTED
        elif positives > 0:
            return GalaxyCandidate.CONFIRMED
        else:
            return GalaxyCandidate.PENDING

    def is_confirmed(self):
        return self.status() == GalaxyCandidate.CONFIRMED

    def is_rejected(self):
        return self.status() == GalaxyCandidate.REJECTED

    def is_disputed(self):
        return self.status() == GalaxyCandidate.DISPUTED

    def is_pending(self):
        return self.status() == GalaxyCandidate.PENDING

    def confirm(self, user):
        Vote.objects.filter(owner=user, galaxy_candidate=self).delete()
        Vote.objects.create(owner=user, galaxy_candidate=self, is_positive=True)

    def reject(self, user):
        Vote.objects.filter(owner=user, galaxy_candidate=self).delete()
        Vote.objects.create(owner=user, galaxy_candidate=self, is_positive=False)

    def positives_votes_count(self):
        return self.votes.filter(is_positive=True).count()

    def negatives_votes_count(self):
        return self.votes.filter(is_positive=False).count()

    def update_thumbnail(self, *args, **options):
        url = create_galaxy_thumbnail(self)
        self.thumbnail.name = url
        self.save()


class Vote(models.Model):

    owner = models.ForeignKey(User, related_name='votes')
    galaxy_candidate = models.ForeignKey(GalaxyCandidate, related_name='votes')
    is_positive = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True)

    @classmethod
    def qualified_votes(cls):
        return cls.objects.filter(owner__is_staff=True)

    class Meta:
        unique_together = (("owner", "galaxy_candidate"),)

    def __unicode__(self):
        t = self.owner_id, self.galaxy_candidate_id
        if self.is_positive:
            pattern = "User %d agrees that galaxy candidate %d is in fact a galaxy"
        else:
            pattern = "User %d thinks that galaxy candidate %d is not a galaxy"
        return pattern % t


class AlienHunted(models.Model):
    hunter = models.ForeignKey(User, related_name='hunted_aliens')
    x = models.DecimalField(max_digits=15, decimal_places=2)
    y = models.DecimalField(max_digits=15, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s: at (%.2f, %.2f)" % (self.timestamp, self.x, self.y)


class Game(object):

    def __init__(self, user):
        self.users = User.objects.all()
        self.current_user = user
        self.first_ten_users = self.users[:10]

        self.aliens = [
            {
                'src': 'ufo.png',
                'size': [100, 61],
            },
            {
                'src': 'spaceship-good-guy.png',
                'size': [150, 76],
            },
            {
                'src': 'good-guy.png',
                'size': [100, 127],
            },
            {
                'src': 'bad-guy.png',
                'size': [69, 100],
            },
        ]

        for alien in self.aliens:
            alien['src'] = staticfiles_storage.url('game/img/aliens/%s' % alien['src'])

        galaxy_candidates = GalaxyCandidate.objects.all()
        self.galaxy_candidates = [self.galaxy_candidate_as_dict(g) for g in galaxy_candidates]

    def user_as_dict(self, obj):
        return {
            'id': obj.id,
            'username': obj.username,
            'displayedName': obj.displayed_name(),
            'levelName': obj.level().name,
            'score': obj.score,
            'isConnectedToFacebook': obj.is_connected_to_facebook(),
            'rankingPosition': obj.ranking_position(include_staff=self.current_user.is_staff),
        }

    def galaxy_candidate_as_dict(self, obj):
        current_user_votes = obj.votes.filter(owner=self.current_user)
        if self.current_user == obj.owner:
            # if the user is the owner of the galaxy, it means he "voted" positively
            current_user_vote = True
        else:
            if current_user_votes.exists():
                # if the user voted, check how his vote was
                current_user_vote = current_user_votes.get().is_positive
            else:
                # if the user didn't vote, the vote is unknown, so is None
                current_user_vote = None

        return {
            'id': obj.id,
            'x': float(obj.x),
            'y': float(obj.y),
            'positives': obj.positives_votes_count(),
            'negatives': obj.negatives_votes_count(),
            'canVote': obj.owner != self.current_user and not current_user_votes.exists(),
            'ownerId': obj.owner.id,
            'thumbnailUrl': obj.thumbnail_url(),
            # Only admins care whether the decision is disputed or not.
            # For players, disputed candidates are presented as confirmed galaxies
            # TODO: bad smell. This logic is repeated in other parts of the code.
            # Those parts can be found by searching for this exactly same comment
            'isConfirmed': obj.is_confirmed() or obj.is_disputed(),
            'isRejected': obj.is_rejected(),
            'currentUserVote': current_user_vote,
        }

    def js_game_module_options(self):
        return {
            'flagType': '_3d',
            'imagesUrl': settings.STATIC_URL + 'game/img/',
            'alienImages': self.aliens,
            'galaxyCandidates': self.galaxy_candidates,
            'ajaxUrl': reverse('game_api'),
            'currentUserId': self.current_user.id,
            'users': [self.user_as_dict(user) for user in self.users],
            'tilesServerUrl': settings.TILE_SERVER_URL,
        }


PUNISHMENT = 50.0
BASE_REWARD = 1000.0
OWNER_PUNISHMENT = 250.0


@receiver(post_save, sender=Vote)
def assign_points(sender, instance, **kwargs):
    vote = instance
    status = vote.galaxy_candidate.status()
    if status in (GalaxyCandidate.CONFIRMED, GalaxyCandidate.REJECTED):
        voting_users = User.objects.filter(
            votes__galaxy_candidate=vote.galaxy_candidate,
            is_staff=False,
        )
        pro = voting_users.filter(votes__is_positive=True)
        con = voting_users.filter(votes__is_positive=False)

        if status == GalaxyCandidate.CONFIRMED:

            total_reward_points = BASE_REWARD + PUNISHMENT * con.count()
            owner_reward_points = total_reward_points / 2
            if pro.count() > 0:
                others_reward_points = (total_reward_points - owner_reward_points) / pro.count()
                others_reward_points = math.ceil(others_reward_points)
                pro.update(_score=F('_score') + others_reward_points)
            con.update(_score=F('_score') - PUNISHMENT)
            owner = vote.galaxy_candidate.owner
            owner.score += math.ceil(owner_reward_points)
            owner.save()

        if status == GalaxyCandidate.REJECTED:

            if con.count() > 0:
                others_reward_points = (
                    (pro.count() * PUNISHMENT + OWNER_PUNISHMENT) / con.count()
                )
                others_reward_points = math.ceil(others_reward_points)
                con.update(_score=F('_score') + others_reward_points)
            pro.update(_score=F('_score') - PUNISHMENT)
            owner = vote.galaxy_candidate.owner
            owner.score -= OWNER_PUNISHMENT
            owner.save()
