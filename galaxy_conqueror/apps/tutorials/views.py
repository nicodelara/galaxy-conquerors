from jsonview.decorators import json_view

from django.contrib.auth.decorators import login_required

from .models import TutorialStats
from utils import log_errors


@json_view
@log_errors
@login_required
def tutorial_closed_ajax(request):

    tutorial_stats, _ = TutorialStats.objects.get_or_create(user=request.user)

    finished = request.POST.get('finished', 'false') == 'true'

    if finished:
        tutorial_stats.done()
    else:
        tutorial_stats.reject()

    return {}
