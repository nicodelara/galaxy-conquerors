from django.conf.urls import patterns, url


urlpatterns = patterns('tutorials.views', *[
    url(r'^tutorial-closed-ajax/$', 'tutorial_closed_ajax', name='tutorial_closed_ajax'),
])
