"""

Taken from https://djangosnippets.org/snippets/2405/

This example assumes you have placed this code into a file called "template_additions.py"
and placed that folder into a "templatetags" folder inside a module.
It can live wherever you need.

Example folder structure:
/lib/
    /__init__.py
    /templatetags/
        /__init__.py
        /template_additions.py
"""

from django import template
from django.conf import settings
from django.utils.html import strip_spaces_between_tags

register = template.Library()


class SmartSpacelessNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        content = self.nodelist.render(context)
        return content if settings.DEBUG else strip_spaces_between_tags(content.strip())


@register.tag
def smart_spaceless(parser, token):
    """
    Removes whitespace between HTML tags, including tab and newline characters,
    but only if settings.DEBUG = False

    Example usage:
        {% load template_additions %}
        {% smart_spaceless %}
            <p>
                <a href="foo/">Foo</a>
            </p>
        {% end_smart_spaceless %}

    This example would return this HTML:

        <p><a href="foo/">Foo</a></p>

    Only space between *tags* is normalized -- not space between tags and text.
    In this example, the space around ``Hello`` won't be stripped:

        {% smart_spaceless %}
            <strong>
                Hello
            </strong>
        {% end_smart_spaceless %}
    """
    nodelist = parser.parse(('end_smart_spaceless',))
    parser.delete_first_token()
    return SmartSpacelessNode(nodelist)
