# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0002_auto_20150407_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='share_url',
            field=models.URLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
