# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0003_notification_share_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='notification',
            name='share_url',
        ),
        migrations.AddField(
            model_name='notification',
            name='fb_feed_description',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='notification',
            name='fb_feed_title',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
    ]
