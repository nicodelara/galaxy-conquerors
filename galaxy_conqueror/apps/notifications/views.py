from jsonview.decorators import json_view

from .models import Notification
from utils import log_errors


@json_view
@log_errors
def mark_as_read(request):
    ids = [int(x) for x in request.POST.getlist('ids[]')]
    Notification.objects.filter(id__in=ids).update(notified=True)
    return {}
