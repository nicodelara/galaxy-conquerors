/*globals
    document,
    jQuery,
    _,
    bootbox,
*/


var notifications = (function (document, $, _, bootbox) {

    'use strict';

    var markAsReadUrl,
        templates;

    function maybeShow(unreadNotifications) {

        // if there are not pending notifications, do nothing
        if (unreadNotifications.length === 0) {
            return;
        }


        var i, ids = [], htmlContent;

        for (i = 0; i < unreadNotifications.length; i += 1) {
            ids.push(unreadNotifications[i].id);
        }

        htmlContent = templates.popup({
            notifications: unreadNotifications,
        });

        bootbox.alert(htmlContent, function () {
            $.ajax(markAsReadUrl, {
                type: 'POST',
                dataType: 'json',
                data: {
                    'ids': ids,
                },
            });
        });
    }

    return {
        init: function (params) {

            markAsReadUrl = params.markAsReadUrl;
            templates = {
                popup: _.template($("#notifications-template").html()),
            };


            $(document).ajaxSuccess(function (evt, xhr) {
                // we check for pending notifications after any ajax response.
                // we know that any ajax request to the game ajax api will return a json response
                // with a unread_notifications attribute which is a list that may be null.
                // however, there are another ajax requests made to other endpoints which
                // won't include that attribute
                var response = JSON.parse(xhr.responseText);
                if (response.unread_notifications !== undefined) {
                    maybeShow(response.unread_notifications);
                }
            });

            maybeShow(params.unreadNotifications);

            $('body').on('click', '.btn-facebook', function () {
                FB.ui({
                    method: 'feed',
                    name: $(this).data('fb_feed_title'),
                    description: $(this).data('fb_feed_description'),
                    link: 'https://cientopolis.lifia.info.unlp.edu.ar',
                    caption: 'Galaxy Conqueror - Cientópolis',
                    //picture: 'http://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror/static/img/cientopolis-grande.png',
		    picture: 'http://conqueror.ddns.net:8000/static/img/galaxy.jpg',
		    //picture: 'https://s10.postimg.org/ij6shidrt/galaxy.jpg',
                });
            });

        }
    };
}(document, jQuery, _, bootbox));
