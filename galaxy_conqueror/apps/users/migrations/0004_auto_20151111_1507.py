# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20150623_0120'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ['-_score']},
        ),
        migrations.RenameField(
            model_name='user',
            old_name='score',
            new_name='_score',
        ),
    ]
