from .models import User, UserActivity, ActivityType


def log_user_activity(backend, details, response, user=None, is_new=False, *args, **kwargs):

    if isinstance(user, User):

        user_activity = UserActivity()
        user_activity.user = user
        user_activity.activity_type = ActivityType.login
        user_activity.save()
