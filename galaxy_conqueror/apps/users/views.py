from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.views import logout_then_login
from users.forms import SignUpForm
from .decorators import log_user_activity


def signup(request):
    form = SignUpForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Su cuenta ha sido creada con exito")
            return redirect('login')
    return render(request, 'users/signup.jinja', {
        'form': form,
    })


def login(request):
    return render(request, 'users/home.jinja')


def profile(request):
    return render(request, 'users/profile.jinja', {
        'user': request.user,
    })


@log_user_activity
def logout(request):
    return logout_then_login(request)
