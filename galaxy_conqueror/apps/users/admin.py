from collections import Counter
from django.contrib import admin
from users.models import User

# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'displayed_name',
        'email',
        'galaxies_marked',
        'galaxy_stats',
        'level',
    )

    def galaxies_marked(self, user):
        return user.galaxy_candidates.count()

    def galaxy_stats(self, user):
        galaxies = user.galaxy_candidates.all()
        counter = Counter()
        for g in galaxies:
            counter[g.status()] += 1
        return ', '.join("%s: %d" % (k, v) for k, v in counter.iteritems())


admin.site.register(User, UserAdmin)
