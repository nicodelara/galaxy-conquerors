from django.conf import settings

def facebook_constants(request):
    return {
        'FACEBOOK_APP_ID': settings.FACEBOOK_APP_ID,
        'FACEBOOK_EXTENDED_PERMISSIONS': settings.FACEBOOK_EXTENDED_PERMISSIONS,
    }