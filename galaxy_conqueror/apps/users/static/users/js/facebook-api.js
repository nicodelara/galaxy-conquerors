/*globals
    window,
    document,
    jQuery,
    FB,
*/

var facebookApi = (function (window, document, $) {

    'use strict';

    var app_id;

    return {
        init: function (params) {

            app_id = params.app_id;
            // scopes = params.scopes;

            // Load the SDK asynchronously
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/es_LA/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.fbAsyncInit = function () {
                FB.init({
                    appId : app_id,
                    cookie : true, // enable cookies to allow the server to access the session
                    xfbml : true, // parse social plugins on this page
                    version : 'v2.2' // use version 2.2
                });
            };
        }

    };
}(window, document, jQuery));