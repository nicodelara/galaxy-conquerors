/*globals
    window,
    document,
    jQuery,
    FB,
*/

var facebookLogin = (function (window, document, $) {

    'use strict';

    var app_id, scopes;

    // function getFacebookName() {
    //     FB.api('/me', function (response) {
    //         return response.name;
    //     });
    // }

    function setConnectedMode() {
        var url;
        FB.api('/me', function (response) {
            $("#signup-btn").hide();
            $("#login-btn").hide();
            $("#facebook-profile").show();
            $('#name').text("Nombre: " + response.name);
            $('#email').text("Email: " + response.id + " " + response.email);
            url = 'http://graph.facebook.com/' + response.id + '/picture?type=normal';
            $('#facebook-profile img').attr('src', url);
        });
    }

    function setAuthorizedButNotLoggedMode() {
        $("#login-btn").show();
        $("#signup-btn").hide();
        $("#facebook-profile").hide();
    }

    function setNotAuthorizedMode() {
        $("#signup-btn").show();
        $("#login-btn").hide();
        $("#facebook-profile").hide();
    }

    // funcion que postea en el muro del usuario: "esto es una prueba"
    function facebookPost() {
        FB.api('/me/feed', 'post', {
            message : 'esto es una prueba!'
        });
    }

    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        switch (response.status) {
        case 'connected':
            setConnectedMode();
            break;
        case 'not_authorized':
            setNotAuthorizedMode();
            break;
        default:
            setAuthorizedButNotLoggedMode();
            break;
        }
    }


    return {
        init: function (params) {

            app_id = params.app_id;
            scopes = params.scopes;

            // Load the SDK asynchronously
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.fbAsyncInit = function () {
                FB.init({
                    appId : app_id,
                    cookie : true, // enable cookies to allow the server to access the session
                    xfbml : true, // parse social plugins on this page
                    version : 'v2.2' // use version 2.2
                });
                $("#login-btn, #signup-btn").click(function () {
                    FB.login(setConnectedMode, {scope: scopes});
                });
                $("#fb-post-btn").click(facebookPost);
                $("#fb-logout-btn").click(function () {
                    FB.logout(setAuthorizedButNotLoggedMode);
                });

                FB.getLoginStatus(statusChangeCallback);
            };
        }

    };
}(window, document, jQuery));