from django.db import models
from django.contrib.auth.models import AbstractUser
from social_auth.db.django_models import UserSocialAuth
from django.contrib.staticfiles.storage import staticfiles_storage
from utils import ChoiceEnum


class User(AbstractUser):

    class Meta:
        ordering = ['-_score']
        # unique_together = ('email', )

    @property
    def score(self):
        return max(0, self._score)

    @score.setter
    def score(self, new_value):
        self._score = max(0, new_value)

    _score = models.IntegerField(default=0)

    def get_achievements(self):
        from badges.models import Achievement
        return Achievement.get_by_user(self)

    def level(self):
        return sorted(self.get_achievements())[-1]

    def ranking_position(self, include_staff=False):
        # note that for users with the same score
        # the same ranking position will be returned
        users = User.objects.all()
        if not include_staff:
            users = users.filter(is_staff=False)
        return users.filter(_score__gt=self.score).count() + 1

    def is_connected_to_facebook(self):
        # TODO: this code just assumes facebook is the only social network in this site, fix it
        return self.social_auth.exists()

    def should_show_facebook_disconnect_button(self):
        return (self.is_connected_to_facebook() and
                UserSocialAuth.allowed_to_disconnect(self, 'facebook'))

    def displayed_name(self):
        return self.get_full_name() or self.username or self.email

    def profile_picture_url(self):
        return staticfiles_storage.url('game/img/user.png')


class ActivityType(ChoiceEnum):
    login = 0
    logout = 1


class UserActivity(models.Model):

    user = models.ForeignKey(User)
    activity_type_value = models.CharField(max_length=2, choices=ActivityType.choices())
    timestamp = models.DateTimeField(auto_now_add=True)

    @property
    def activity_type(self):
        return ActivityType(int(self.activity_type_value))

    @activity_type.setter
    def activity_type(self, activity_type):
        self.activity_type_value = activity_type.value

    def __unicode__(self):
        return "User %d %s at %s" % (self.user_id, self.activity_type.name, self.timestamp)
