# -*- coding: utf-8 -*-
from functools import cmp_to_key
from django.conf import settings
from django.dispatch import receiver
from django.utils.text import slugify
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from users.models import User
from game import models as game_models
from api_proxy import proxy
from notifications.models import Notification
from utils import html_entities_to_unicode


def _compare(a1, a2):
    attrs = zip(a1._criteria_tuple(), a2._criteria_tuple())
    for a1_attr, a2_attr in attrs:
        if a1_attr < a2_attr:
            return -1
        if a1_attr > a2_attr:
            return 1
    return 0


class Achievement(cmp_to_key(_compare)):

    achievement_by_id = None
    achievement_by_slug = None

    @classmethod
    def update(cls):
        cls.achievement_by_id = dict()
        cls.achievement_by_slug = dict()
        for json_data in proxy.get_achievements():
            achievement = cls(json_data)
            cls.achievement_by_id[achievement.id] = achievement
            cls.achievement_by_slug[achievement.slug] = achievement

    @classmethod
    def all(cls):
        if cls.achievement_by_id is None:
            cls.update()
        return cls.achievement_by_id.values()

    @classmethod
    def get_by_id(cls, id_badge_class):
        if cls.achievement_by_id is None:
            cls.update()
        return cls.achievement_by_id[id_badge_class]

    @classmethod
    def get_by_slug(cls, slug):
        if cls.achievement_by_id is None:
            cls.update()
        return cls.achievement_by_slug[slug]

    @classmethod
    def get_by_user(cls, user):
        ids = {a['id_badge_class'] for a in proxy.get_achievements_by_user(user.email)[user.email]}
        return [cls.get_by_id(id_) for id_ in ids]

    @classmethod
    def deliver(cls, user):
        for achievement in cls.all():
            if not achievement.is_own_by(user):
                if achievement.is_deserved_by(user):
                    achievement.deliver_to_user(user)

    def __init__(self, json_data):
        super(Achievement, self).__init__(self)
        self.id = json_data['id_badge_class']
        self.image_url = json_data['imageUrl']
        if settings.SITE_URL not in self.image_url:
            self.image_url = self.image_url.replace(
                'https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror',
                'http://' + settings.SITE_URL,
            )

        self.name = html_entities_to_unicode(json_data['name'])
        self.slug = slugify(self.name)
        self.description = json_data['description']
        self.points_earned_required = int(json_data['criteria'][0]['note'])
        self.aliens_hunted_required = int(json_data['criteria'][1]['note'])
        self.votes_required = int(json_data['criteria'][2]['note'])
        self.galaxies_marked_required = int(json_data['criteria'][3]['note'])
        self.galaxies_discovered_required = int(json_data['criteria'][4]['note'])

    def deliver_to_user(self, user):
        proxy.create_achievement_acquisition(self.id, user.email)
        Notification.create_achievement_notification(self, user)

    def get_acquisition_url(self, owner):
        return settings.SITE_URL + reverse('show_achievement', args=[
            owner.id,
            slugify(owner.displayed_name()),
            slugify(self.name),
        ])

    def is_deserved_by(self, user):

        if user.score < self.points_earned_required:
            return False

        hunted_aliens = game_models.AlienHunted.objects.filter(hunter=user)
        if self.aliens_hunted_required > 0 and hunted_aliens.count() < self.aliens_hunted_required:
            return False

        votes = game_models.Vote.objects.filter(owner=user)
        if self.votes_required > 0 and votes.count() < self.votes_required:
            return False

        user_candidates = game_models.GalaxyCandidate.objects.filter(owner=user)
        if self.galaxies_discovered_required > 0:
            if user_candidates.count() < self.galaxies_marked_required:
                return False

        if self.galaxies_discovered_required > 0:
            confirmed_count = 0
            for candidate in user_candidates:
                # Only admins care whether the decision is disputed or not.
                # For players, disputed candidates are presented as confirmed galaxies
                # TODO: bad smell. This logic is repeated in other parts of the code.
                # Those parts can be found by searching for this exactly same comment
                if candidate.is_confirmed() or candidate.is_disputed():
                    confirmed_count += 1
            if confirmed_count < self.galaxies_discovered_required:
                return False
        return True

    def is_own_by(self, user):
        response = proxy.achievement_is_own_by_user(self.id, user.email)
        return response['issuedOn'] is not None

    def _criteria_tuple(self):
        return (
            self.points_earned_required,
            self.aliens_hunted_required,
            self.votes_required,
            self.galaxies_marked_required,
            self.galaxies_discovered_required,
        )

    @property
    def description_for_fb_feed(self):
        desc = "He obtenido una nueva insignia por "

        criteria_tuple = self._criteria_tuple()
        if all(map(lambda x: x == 0, criteria_tuple)):
            return desc + "jugar a Galaxy Conqueror"

        def generate_parts():

            def decide_by_amount(amount, singular_msg, plural_msg_pattern):
                if amount == 0:
                    return None
                if amount == 1:
                    return singular_msg
                return plural_msg_pattern % amount

            if not self.galaxies_discovered_required:
                yield decide_by_amount(
                    self.galaxies_marked_required,
                    "haber marcado una galaxia por primera vez",
                    "haber marcado %d galaxias",
                )

            yield decide_by_amount(
                self.galaxies_discovered_required,
                "haber descubierto mi primera galaxia",
                "haber descubierto %d galaxias",
            )

            yield decide_by_amount(
                self.votes_required,
                "haber votado por primera vez",
                "haber votado %d veces",
            )

            yield decide_by_amount(
                self.aliens_hunted_required,
                "capturar mi primer extraterrestre",
                "capturar %d extraterrestres",
            )

            yield decide_by_amount(
                self.points_earned_required,
                "obtener mi primer punto",
                "obtener %d puntos",
            )

        parts = [part for part in generate_parts() if part is not None]
        first_parts, last_part = parts[:-1], parts[-1]
        enumeration = [', '.join(first_parts), last_part]
        enumeration = [x for x in enumeration if x]
        return desc + " y ".join(enumeration)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self.name.encode('utf-8'))

python_2_unicode_compatible(Achievement)


@receiver(post_save, sender=game_models.AlienHunted)
def alien_hunted(sender, instance, created, **kwargs):
    if created:
        user = instance.hunter
        Achievement.deliver(user)


@receiver(post_save, sender=game_models.GalaxyCandidate)
def galaxy_marked(sender, instance, created, **kwargs):
    if created:
        galaxy_candidate = instance
        Achievement.deliver(galaxy_candidate.owner)


@receiver(post_save, sender=game_models.Vote)
def galaxy_confirmed(sender, instance, created, **kwargs):
    vote = instance
    if vote.is_positive and vote.owner.is_staff:
        Achievement.deliver(vote.galaxy_candidate.owner)
        for other_vote in game_models.Vote.objects.filter(galaxy_candidate=vote.galaxy_candidate):
            user = other_vote.owner
            if not user.is_staff:
                Achievement.deliver(user)


@receiver(post_save, sender=game_models.Vote)
def user_voted(sender, instance, created, **kwargs):
    if created:
        vote = instance
        user = vote.owner
        Achievement.deliver(user)


@receiver(post_save, sender=User)
def points_earned(sender, instance, created, **kwargs):
    user = instance
    Achievement.deliver(user)
