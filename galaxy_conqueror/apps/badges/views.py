from django.conf import settings
from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.utils.text import slugify
from .models import Achievement
from users.models import User


def show_achievement(request, user_id, user_slug, badge_slug):
    user = User.objects.get(id=user_id)
    if slugify(user.displayed_name()) != user_slug:
        raise Http404("User not found")
    return render(request, 'badges/show.jinja', {
        'owner': user,
        'achievement': Achievement.get_by_slug(badge_slug),
    })


def image(request, badge_slug):
    with open(settings.PROJECT_DIR + '/apps/badges/static/badges/img/%s.png' % badge_slug) as f:
        return HttpResponse(f.read(), content_type="image/png")
