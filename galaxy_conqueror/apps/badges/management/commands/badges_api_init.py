import json
import requests
from django.core.management.base import BaseCommand
from django.conf import settings


class Command(BaseCommand):
    help = ("Sends a POST request to the API Badges endpoint "
            "which initializes the set of badges. ")

    def handle(self, *args, **options):

        id_app = raw_input("Enter the app name (i.e. galaxy_conqueror): ")

        json_dict = {
            'id_app': id_app,
            'name': "GalaxyConqueror",
            'url': "https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror",
        }
        with open(settings.BASE_DIR + '/badges.json') as badges_file:
            json_dict['badges'] = json.load(badges_file)

        response = requests.post("https://cientopolis.lifia.info.unlp.edu.ar/badges-api/carga-json",
                                 json=[json_dict],
                                 verify=False)
        print response.status_code
        if response.status_code == 200:
            print response.json()
        else:
            print response.content
