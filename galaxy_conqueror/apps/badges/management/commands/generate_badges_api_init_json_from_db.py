import json
from django.core.management.base import BaseCommand
from django.utils.text import slugify
from badges.models import Badge


URL = "https://cientopolis.lifia.info.unlp.edu.ar/galaxy-conqueror"


class Command(BaseCommand):
    help = "Generates the Badges API initialization json file from the badges in the database"

    def handle(self, *args, **options):
        badges_init_dict = {
            'id_app': "galaxy_conqueror",
            'name': "Galaxy Conqueror",
            'url': URL + "/",
            'badges': [{
                'name': badge.name,
                'imageUrl': "%s/badges/images/%s.png" % (URL, slugify(badge.name)),
                'criteriaUrl': "%s/badges/%s" % (URL, slugify(badge.name)),
                'description': badge.description,
            } for badge in Badge.objects.all()]
        }
        self.stdout.write(json.dumps([badges_init_dict]))
