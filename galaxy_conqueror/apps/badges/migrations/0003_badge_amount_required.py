# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0002_badge_code_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='badge',
            name='amount_required',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
