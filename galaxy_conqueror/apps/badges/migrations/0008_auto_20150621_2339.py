# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0007_auto_20150428_0001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='badgeacquisition',
            name='badge',
            field=models.ForeignKey(related_name='acquisitions', to='badges.Badge'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='badgeacquisition',
            name='owner',
            field=models.ForeignKey(related_name='achievements', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
