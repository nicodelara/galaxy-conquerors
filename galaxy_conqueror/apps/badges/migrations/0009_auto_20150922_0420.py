# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0008_auto_20150621_2339'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='badge',
            name='owners',
        ),
        migrations.RemoveField(
            model_name='badgeacquisition',
            name='badge',
        ),
        migrations.DeleteModel(
            name='Badge',
        ),
        migrations.RemoveField(
            model_name='badgeacquisition',
            name='owner',
        ),
        migrations.DeleteModel(
            name='BadgeAcquisition',
        ),
    ]
