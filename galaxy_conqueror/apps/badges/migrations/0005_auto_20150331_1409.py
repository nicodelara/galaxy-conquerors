# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0004_badge_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='badge',
            name='code_name',
        ),
        migrations.AlterField(
            model_name='badge',
            name='category',
            field=models.CharField(max_length=1, choices=[(b'h', 'Insignia de cazador'), (b'e', 'Insignia de explorador'), (b'c', 'Insignia de ciudadano'), (b'p', 'Insignia de jugador')]),
            preserve_default=True,
        ),
    ]
