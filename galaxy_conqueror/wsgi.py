"""
WSGI config for galaxy_conqueror project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os

settings_module_by_host = {
    'cientopolis_org': 'galaxy_conqueror.settings.server_cientopolis',
    'cientopolis': 'galaxy_conqueror.settings.server_ciencia'
}

host = os.uname()[1]

settings_module = settings_module_by_host.get(host, 'galaxy_conqueror.settings.development')
os.environ['DJANGO_SETTINGS_MODULE'] = settings_module

os.environ['HTTPS'] = "on"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
